const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

io.on('connection', (socket) => {
	socket.on('new_client',(pseudo) => {
		socket.pseudo = pseudo;
		socket.broadcast.emit('new_client', `${pseudo} vient de se connecter.`);
	});

	socket.on('new_message', (message) => {
		io.emit('new_message', {text: message, author: socket.pseudo});
	});

	socket.on('disconnecting', (reason) => {
		socket.broadcast.emit('client_left', `${socket.pseudo} vient de partir.`);
	});
});

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
});

http.listen(3000, () => {
	console.log('Server listening on port http://localhost:3000')
})